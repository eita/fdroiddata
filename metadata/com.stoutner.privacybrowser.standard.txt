Categories:Internet
License:GPLv3+
Author Name:Soren Stoutner
Author Email:soren@stoutner.com
Web Site:https://www.stoutner.com/privacy-browser
Source Code:https://git.stoutner.com/?p=PrivacyBrowser.git;a=summary
Issue Tracker:https://redmine.stoutner.com/projects/privacy-browser/issues
Changelog:https://www.stoutner.com/privacy-browser/changelog/
Donate:https://www.stoutner.com/privacy-browser/contributors/

Auto Name:Privacy Browser
Summary:A web browser that respects your privacy
Description:
Privacy Browser is a web browser that protects your privacy by giving you easy
control over the use of JavaScript, DOM storage, and cookies. Many websites use
these technologies to track users between visits and between sites. Tapping the
privacy shield toggles whether JavaScript is enabled and reloads the WebView.
When JavaScript is disabled, not only are privacy and security increased, but
web pages load faster with less bandwidth consumption.

Privacy Browser uses Android's built-in WebView to render web pages. As such, it
works best if you have updated to the latest version of WebView.
.

Repo Type:git
Repo:git://git.stoutner.com/git/PrivacyBrowser.git

Build:1.3-standard,4
    commit=v1.3-standard
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.4-standard,5
    commit=v1.4-standard
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.5,6
    commit=v1.5
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.6,7
    commit=v1.6
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.7,8
    commit=v1.7
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.8,9
    commit=v1.8
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.9,10
    disable=outdated, failing build
    commit=v1.9
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.9.1,11
    commit=v1.9.1
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.10,12
    commit=v1.10
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.11,13
    commit=v1.11
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.11
Current Version Code:13
